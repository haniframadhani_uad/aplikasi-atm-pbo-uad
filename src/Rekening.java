import java.io.*;
import java.util.Scanner;

public class Rekening {
  private String[] sementara;
  private String data;
  private String noRekening;
  private String[] noRekeningSementara;
  private String nama;
  private String[] namaSementara;
  private int pin;
  private String[] pinSementara;
  private int dana;
  private String[] danaSementara;

  Rekening() {
    try {
      File kartu = new File("src/kartuAtm.txt");
      Scanner sementaraSaja = new Scanner(kartu);
      while (sementaraSaja.hasNextLine()) {
        data = sementaraSaja.nextLine();
      }
      sementaraSaja.close();
    } catch (FileNotFoundException e) {
      System.out.println("file tidak ditemukan");
      System.out.println(e);
      e.printStackTrace();
    }
    sementara = data.split(";");
    namaSementara = sementara[0].split(" : ");
    noRekeningSementara = sementara[1].split(" : ");
    pinSementara = sementara[2].split(" : ");
    danaSementara = sementara[3].split(" : ");
    nama = namaSementara[1];
    noRekening = noRekeningSementara[1];
    pin = Integer.parseInt(pinSementara[1]);
    dana = Integer.parseInt(danaSementara[1]);
  }

  public int getDana() {
    return dana;
  }

  public String getNama() {
    return nama;
  }

  public int getPin() {
    return pin;
  }

  public String getNoRekening() {
    return noRekening;
  }

  public void setDana(int danaParameter) {
    this.dana = danaParameter;
  }

  public void selesai() {
    try {
      FileWriter kartu = new FileWriter("src/kartuAtm.txt");
      kartu.write(
          "nama : " + this.nama + ";no kartu atm : " + this.noRekening + ";no pin : " + this.pin + ";saldo : "
              + this.dana + ";");
      kartu.close();
    } catch (Exception e) {
      System.out.println("file tidak ditemukan");
      System.out.println(e);
      e.printStackTrace();
    }
  }
}
