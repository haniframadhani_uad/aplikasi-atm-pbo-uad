import java.util.Scanner;

public class Atm {
  private int pin;
  private int dana;
  private boolean login = false;

  Atm(int p, int d) {
    this.pin = p;
    this.dana = d;
  }

  public void seeDana() {
    System.out.println("dana : " + dana);
  }

  public int getDana() {
    return this.dana;
  }

  public void menarikDana(int tarik) {
    if (isMoreThanAvailable(tarik)) {
      System.out.println("saldo anda tidak mencukupi");
      return;
    }
    System.out.println("memproses transaksi");
    this.dana -= tarik;
    System.out.println("transaksi berhasil");
  }

  private boolean isMoreThanAvailable(int tarik) {
    return tarik > this.dana ? true : false;
  }

  private boolean isZeroOrBelow(int setor) {
    return setor <= 0 ? true : false;
  }

  public void menyetorDana(int setor) {
    if (isZeroOrBelow(setor)) {
      System.out.println("saldo yang disetor tidak boleh nol atau lebih kecil");
      return;
    }
    System.out.println("memproses transaksi");
    this.dana += setor;
    System.out.println("transaksi berhasil");
  }

  public boolean isLogged() {
    return (login == true) ? true : false;
  }

  public void inputPin() {
    boolean salah = true;
    int pinInput;
    Scanner masuk = new Scanner(System.in);
    while (salah) {
      System.out.print("masukkan pin : ");
      pinInput = masuk.nextInt();
      if (pinInput == this.pin) {
        login = true;
        salah = true;
        System.out.print("\033[H\033[2J");
        return;
      }
      System.out.println("pin salah");
    }
  }
}
