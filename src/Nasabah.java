public class Nasabah {
  private String nama;
  private String noRekening;
  private int pin;
  private int dana;

  Rekening debit = new Rekening();

  public Nasabah() {
    nama = debit.getNama();
    noRekening = debit.getNoRekening();
    pin = debit.getPin();
    dana = debit.getDana();
  }

  public int getDana() {
    return dana;
  }

  public String getNama() {
    return nama;
  }

  public int getPin() {
    return pin;
  }

  public String getNoRekening() {
    return noRekening;
  }

  public void selesai(int danaParameter) {
    debit.setDana(danaParameter);
    debit.selesai();
  }

}
