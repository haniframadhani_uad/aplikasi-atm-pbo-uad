import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Nasabah nasabah = new Nasabah();
		int dana = nasabah.getDana();
		int pin = nasabah.getPin();
		Atm mesinAtm = new Atm(pin, dana);
		int pilihan;
		Scanner input = new Scanner(System.in);
		boolean lanjut = true;
		mesinAtm.inputPin();
		if (mesinAtm.isLogged()) {
			while (lanjut) {
				System.out.print("1. menarik saldo\n2. menyetor saldo\n3. melihat saldo\n4. selesai\nmasukkan pilihan : ");
				pilihan = input.nextInt();
				switch (pilihan) {
					case 1:
						System.out.print("\033[H\033[2J");
						System.out.print("masukkan jumlah yang akan ditarik : ");
						pilihan = input.nextInt();
						mesinAtm.menarikDana(pilihan);
						break;
					case 2:
						System.out.print("\033[H\033[2J");
						System.out.print("masukkan jumlah yang akan disetor : ");
						pilihan = input.nextInt();
						mesinAtm.menyetorDana(pilihan);
						break;
					case 3:
						System.out.print("\033[H\033[2J");
						mesinAtm.seeDana();
						break;
					case 4:
						System.out.println("terima kasih telah transaksi");
						lanjut = false;
						break;
					default:
						System.out.println("pilihan tidak ada");
						break;
				}
			}
		}
		input.close();
		nasabah.selesai(mesinAtm.getDana());
	}
}
